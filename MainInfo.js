import React from 'react';
import {
  View,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator, StackActions, NavigationActions} from 'react-navigation';
import { connect } from "react-redux";
import { addMainInfo } from "./actions/index";
import * as firebase from 'firebase'


const mapDispatchToProps = dispatch => {
  return {
    addMainInfo: info => dispatch(addMainInfo(info))
  }
}

const mapStateToProps = state => {
  return { currentState: state.mainInfo };
};


class MainInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nameCompetion: '',
      hall: '',
      city: '',
      division: '',
      matchNo: '',
      date: '',
      time: '',
      category: '',
    }
  }

  static navigationOptions = {
    title: "Main Info"
  }

  handleContinue(){
    this.props.navigation.navigate('Referees',
      {nameCompetion: this.state.nameCompetion, matchNo: this.state.matchNo})

    firebase.database().ref(this.state.nameCompetion + '/' + this.state.matchNo + '/' + 'MainInfo').set({
      CompetitionName: this.state.nameCompetion,
      hall: this.state.hall,
      city: this.state.city,
      division: this.state.division,
      matchNo: this.state.matchNo,
      date: this.state.date,
      time: this.state.time,
      category: this.state.category
    })

    this.props.addMainInfo({
      nameCompetion: this.state.nameCompetion,
      hall: this.state.hall,
      city: this.state.city,
      division: this.state.division,
      matchNo: this.state.matchNo,
      date: this.state.date,
      time: this.state.time,
      category: this.state.category
    });
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>

        <Text style={styles.title}>
          Main info of the event
        </Text>

        <TextInput required value={this.state.nameCompetion} onChangeText={(text) => this.setState({nameCompetion: text})}
          placeholder={'Name of the competion'} placeholderTextColor="black"  style={styles.input}/>

        <TextInput required value={this.state.hall} onChangeText={(text) => this.setState({hall: text})}
          placeholder={'Hall'} placeholderTextColor="black" style={styles.input}/>

        <TextInput required value={this.state.city} onChangeText={(text) => this.setState({city: text})}
          placeholder={'City'} placeholderTextColor="black" style={styles.input}/>

        <TextInput required value={this.state.division} onChangeText={(text) => this.setState({division: text})}
          placeholder={'Division'} placeholderTextColor="black" style={styles.input}/>

        <TextInput required value={this.state.matchNo} onChangeText={(text) => this.setState({matchNo: text})}
          placeholder={'Match No'} placeholderTextColor="black" style={styles.input}/>

        <TextInput required value={this.state.date} onChangeText={(text) => this.setState({date: text})}
          placeholder={'Date'} placeholderTextColor="black" style={styles.input}/>

        <TextInput required value={this.state.time} onChangeText={(text) => this.setState({time: text})}
          placeholder={'Time'} placeholderTextColor="black" style={styles.input}/>

        <TextInput required value={this.state.category} onChangeText={(text) => this.setState({category: text})}
          placeholder={'Category'} placeholderTextColor="black" style={styles.input}/>

        <TouchableOpacity onPress={this.handleContinue.bind(this)}>
          <Text style={styles.button}>
            Continue
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView >
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainInfo)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#edf3fc'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  }
});
