import React from 'react';
import {
  View,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator} from 'react-navigation';

import * as firebase from 'firebase'

class MainScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    title: "Main"
  }

  render() {
    return (
      <View style={styles.container}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('MainInfo')} style={styles.button}>
            <Text style={styles.text}>
                Start a new Scoresheet
            </Text>
          </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#edf3fc'
  },

  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 100,
    marginBottom: 10,
    // textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9',
  },

  text: {
    marginTop: 25,
    textAlign: 'center'
  }
});
  export default MainScreen
