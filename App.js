import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation';

import LoginScreen from './LoginScreen'
import SignUpScreen from './SignUpScreen'
import MainScreen from './MainScreen'
import MainInfo from './MainInfo'
import Referees from './Referees'
import TeamA from './TeamA'
import TeamB from './TeamB'
import ServiceOrderTeamA from './ServiceOrderTeamA'
import SendData from './SendData'
import Set2 from './Set2'
import * as firebase from 'firebase'
import {Provider} from 'react-redux'
import store from "./store/index";
import { connect } from "react-redux";
import ServiceOrderTeamB from './ServiceOrderTeamB'
import Points from './Points'
import Substitutions from './Substitutions'
import TimeoutsComponent from './TimeoutsComponent'

// const RootStack = createStackNavigator(
//   {
//       Login: {
//         screen: createMaterialTopTabNavigator({
//           TeamA: TeamA,
//           TeamB: TeamB
//         })
//       },
//       SignUp: SignUpScreen,
//       Main: MainScreen,
//       MainInfo: MainInfo,
//       Referees: Referees,
//       TeamA: TeamA,
//       ServiceOrderTeamA: ServiceOrderTeamA,
//       SendData: SendData,
//       Set1: Set1,
//       Set2: Set2
//   },
//   {
//     initialRouteName: 'Login'
//   }
// )

const RootStack = createStackNavigator(
  {
      Login: {screen: createMaterialTopTabNavigator({
        Subs: Substitutions,
        Points: Points,
        Timeouts: TimeoutsComponent
      })},
      SignUp: SignUpScreen,
      Main: MainScreen,
      MainInfo: MainInfo,
      Referees: Referees,
      TeamA: {screen: createMaterialTopTabNavigator({
        TeamA: TeamA,
        TeamB: TeamB
      })},
      ServiceOrderTeamA: {screen: createMaterialTopTabNavigator({
        ServiceOrderTeamA: ServiceOrderTeamA,
        ServiceOrderTeamB: ServiceOrderTeamB
      })},
      Set1: {screen: createMaterialTopTabNavigator({
        Subs: Substitutions,
        Points: Points,
        Timeouts: TimeoutsComponent
      })},
      Set2: Set2
  },
  {
    initialRouteName: 'Login'
  }
)


class App extends React.Component {
  componentWillMount(){
    firebase.initializeApp({
      apiKey: "AIzaSyBhmwUWVIbreWsJuy6-XkwSpVarZC5b6oQ",
      authDomain: "volleyapp-66bb0.firebaseapp.com",
      databaseURL: "https://volleyapp-66bb0.firebaseio.com",
      projectId: "volleyapp-66bb0",
      storageBucket: "volleyapp-66bb0.appspot.com",
      messagingSenderId: "387817164677"
    })
  }

  render() {
    return(
          <Provider store={store}>
                <RootStack />
          </Provider>
    )
  }
}

export default App
