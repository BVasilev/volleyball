import React from 'react';
import {
  View,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator, StackActions, NavigationActions} from 'react-navigation';

import * as firebase from 'firebase'

import LoginComponent from './LoginComponent'

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  static navigationOptions = {
    title: "Login"
  }

  resetNavigation = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Main' })],
      });
    this.props.navigation.dispatch(resetAction);
  }

  handleLogin = () =>{
    firebase
      .auth()
      .signInWithEmailAndPassword(this.state.username, this.state.password)
      .then(this.resetNavigation)
      .catch((error) => alert(error))
  }


  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>

        <Text style={styles.title}>
          Welcome to the Volleyball Scoresheet
        </Text>

        <TextInput required value={this.state.username} onChangeText={(text) => this.setState({username: text})} placeholder={'Username'} placeholderTextColor="black"  style={styles.input}/>
        <TextInput required value={this.state.password} onChangeText={(password) => this.setState({password})} placeholder={'Password'} placeholderTextColor="black" secureTextEntry={true} style={styles.input}/>

        <TouchableOpacity onPress={this.handleLogin}>
          <Text style={styles.button}>
            Login
          </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
          <Text style={styles.button}>
            Not a member? Sign Up
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#edf3fc'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 100
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  }
});

export default LoginScreen
