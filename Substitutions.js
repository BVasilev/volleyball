import React from 'react';
import {
  View,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator, StackActions, NavigationActions, withNavigation} from 'react-navigation';

import * as firebase from 'firebase'

import { connect} from "react-redux";
import {bindActionCreators} from 'redux'
import {addSubA, addSubB} from './actions/index'


// TO DO:
// at the moment only one sub can be made, because when updating the state in the store, the subs object is being
// overriten. needs to be changed to accept more than one sub, check if it is a double change(returning player),
// it should make sure that there are no more than 6 subs made for the entire set

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({addSubA: addSubA, addSubB: addSubB}, dispatch)
  }
}

const mapStateToProps = state => {
  return { currentState: state };
};

class Substitutions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      teamA_Out: '',
      teamA_In: '',
      teamB_Out: '',
      teamB_In: '',
      countSubsA: 1,
      countSubsB: 1,
    }
  }

  static navigationOptions = {
    title: "Substitutions"
  }


  onEndEditingSubsA(){

    firebase.database().ref(this.props.navigation.getParam('nameCompetion')
              + '/' + this.props.navigation.getParam('matchNo') + '/' + 'Set' + 1 + '/'
              + 'Subs' + '/' + 'TeamA' + '/' + this.state.countSubsA + '_Sub').set({

                PointsA: this.props.currentState.Set1.pointsA,
                PointsB: this.props.currentState.Set1.pointsB,
                Out: this.state.teamA_Out,
                In: this.state.teamA_In
    })

    this.props.addSubA({
        PointsA: this.props.currentState.Set1.pointsA,
        PointsB: this.props.currentState.Set1.pointsB,
        In: this.state.teamA_In,
        Out: this.state.teamA_Out
    })

    this.setState({
      countSubsA: this.state.countSubsA + 1,
      teamA_In: '',
      teamA_Out: ''
    })
  }

  onEndEditingSubsB(){

    firebase.database().ref(this.props.navigation.getParam('nameCompetion')
              + '/' + this.props.navigation.getParam('matchNo') + '/' + 'Set' + 1 + '/'
              + 'Subs' + '/' + 'TeamB' + '/' + this.state.countSubsB + '_Sub').set({

                PointsA: this.props.currentState.Set1.pointsA,
                PointsB: this.props.currentState.Set1.pointsB,
                Out: this.state.teamB_Out,
                In: this.state.teamB_In
    })

    this.props.addSubB({
        PointsA: this.props.currentState.Set1.pointsA,
        PointsB: this.props.currentState.Set1.pointsB,
        In: this.state.teamB_In,
        Out: this.state.teamB_Out
    })

    this.setState({
      countSubsB: this.state.countSubsB + 1,
      teamB_In: '',
      teamB_Out: ''
    })
  }


  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>

        <Text style={styles.title}>
          Substitutions
        </Text>

        <Text> Team A </Text>

        <TextInput value={this.state.teamA_Out} onChangeText={(text) => this.setState({teamA_Out: text})}
          placeholder={'N of starting player'} placeholderTextColor="black" style={styles.input}/>

        <TextInput value={this.state.teamA_In} onChangeText={(text) => this.setState({teamA_In: text})}
          onEndEditing={this.onEndEditingSubsA.bind(this)}
          placeholder={'N of entering player'} placeholderTextColor="black" style={styles.input}/>

        <Text> Team B </Text>

        <TextInput value={this.state.teamB_Out} onChangeText={(text) => this.setState({teamB_Out: text})}
          placeholder={'N of starting player'} placeholderTextColor="black" style={styles.input}/>

        <TextInput value={this.state.teamB_In} onChangeText={(text) => this.setState({teamB_In: text})}
          onEndEditing={this.onEndEditingSubsB.bind(this)}
          placeholder={'N of entering player'} placeholderTextColor="black" style={styles.input}/>

      </KeyboardAvoidingView >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#edf3fc'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  }
});

export default connect(mapStateToProps, mapDispatchToProps) (Substitutions)
