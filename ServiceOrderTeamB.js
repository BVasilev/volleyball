import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator, StackActions, NavigationActions, createMaterialTopTabNavigator} from 'react-navigation';

import { connect } from "react-redux";
import { addServiceOrderTeamB } from "./actions/index";
import * as firebase from 'firebase'


const mapDispatchToProps = dispatch => {
  return {
    addServiceOrderTeamB: serviceB => dispatch(addServiceOrderTeamB(serviceB))
  }
}

const mapStateToProps = state => {
  return { currentState: state };
};

class ServiceOrderTeamB extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      I: '',
      II: '',
      III: '',
      IV: '',
      V: '',
      VI: ''
    }
  }

  static navigationOptions = {
    title: "Service order team B"
  }

  handleContinue(){
     this.props.navigation.navigate('Set1')

    firebase.database().ref(this.props.currentState.mainInfo[0].nameCompetion
              + '/' + this.props.currentState.mainInfo[0].matchNo + '/' + 'ServiceOrderTeamB').set({

      I: this.state.I,
      II: this.state.II,
      III: this.state.III,
      IV: this.state.IV,
      V: this.state.V,
      VI: this.state.VI,
    })

    addServiceOrderTeamB({
      I: this.state.I,
      II: this.state.II,
      III: this.state.III,
      IV: this.state.IV,
      V: this.state.V,
      VI: this.state.VI,
    })
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>

        <Text style={styles.title}>Team A service order</Text>

        <Text style={styles.instructions}>Please fill in the 6 positions with the number of the starting player. </Text>

          <TextInput   value={this.state.I} onChangeText={(text) => this.setState({I: text})}
            placeholder={'I'} placeholderTextColor="black"  style={styles.input}/>

          <TextInput   value={this.state.II} onChangeText={(text) => this.setState({II: text})}
            placeholder={'II'} placeholderTextColor="black" style={styles.input}/>

          <TextInput   value={this.state.III} onChangeText={(text) => this.setState({III: text})}
            placeholder={'III'} placeholderTextColor="black" style={styles.input}/>

          <TextInput   value={this.state.IV} onChangeText={(text) => this.setState({IV: text})}
            placeholder={'IV'} placeholderTextColor="black" style={styles.input}/>

          <TextInput   value={this.state.V} onChangeText={(text) => this.setState({V: text})}
            placeholder={'V'} placeholderTextColor="black" style={styles.input}/>

          <TextInput   value={this.state.VI} onChangeText={(text) => this.setState({VI: text})}
            placeholder={'VI'} placeholderTextColor="black" style={styles.input}/>

       <TouchableOpacity onPress={this.handleContinue.bind(this)}>
         <Text style={styles.button}>
           Continue
         </Text>
       </TouchableOpacity>
   </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#edf3fc'
  },
  instructions:{
    marginBottom: 20
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  }
});


export default connect(mapStateToProps, mapDispatchToProps) (ServiceOrderTeamB)
