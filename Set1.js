import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import Substitutions from './Substitutions'
import TimeoutsComponent from './TimeoutsComponent'
import { RNNumberStepper } from 'react-native-number-stepper';
import SendData from './SendData'
import Set2 from './Set2'
import {createStackNavigator, TabNavigator, StackActions, NavigationActions, createMaterialTopTabNavigator, withNavigation} from 'react-navigation';

import * as firebase from 'firebase'



class Set1 extends React.Component{
  constructor(props) {
    super(props)
    }

  handleContinue(){

    this.props.navigation.navigate('Set2',
        {nameCompetion: this.props.navigation.getParam('nameCompetion'), matchNo: this.props.navigation.getParam('matchNo')})
  }

  render() {
    return (
      <TabMenu screenProps={{ pointsAG: pointsA, pointsBG: pointsB, set: set, handleContinue: this.handleContinue.bind(this)}}/>
    );
  }
}

export default withNavigation (Set1)


class ContinueButton extends React.Component{

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.props.handleContinue}>
           <Text style={styles.button}>
             Continue to second set
           </Text>
         </TouchableOpacity>
       </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer:{
    paddingBottom: 10,
    paddingVertical: 5,
  },
  container:{
    marginTop: 50,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  stepper:{
    marginRight: 10
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 5,
    textAlign: 'center'
  },
  text: {
    fontSize: 27,
    fontWeight: 'bold',
    marginBottom: 20,
    marginLeft: 10,
    marginTop: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 25,
    marginTop: 5
  },
  input: {
    width: 330,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  }
});
