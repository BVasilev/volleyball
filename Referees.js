import React from 'react';
import {
  View,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator, StackActions, NavigationActions} from 'react-navigation';
import { connect } from "react-redux";
import { addReferees } from "./actions/index";
import * as firebase from 'firebase'

const mapDispatchToProps = dispatch => {
  return {
    addReferees: referees => dispatch(addReferees(referees))
  }
}

const mapStateToProps = state => {
  return { currentState: state };
};


class Referees extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstReferee: '',
      secondReferee: '',
      lineJudge1: '',
      lineJudge2: '',
      scorekeeper: '',
    }
  }

  static navigationOptions = {
    title: "Referees"
  }

  handleContinue(){
    this.props.navigation.navigate('TeamA')

    firebase.database().ref(this.props.currentState.mainInfo[0].nameCompetion
              + '/' + this.props.currentState.mainInfo[0].matchNo + '/' + 'Referees').set({

      firstReferee: this.state.firstReferee,
      secondReferee: this.state.secondReferee,
      lineJudge1: this.state.lineJudge1,
      lineJudge2: this.state.lineJudge2,
      scorekeeper: this.state.scorekeeper,
    })

    this.props.addReferees({
      firstReferee: this.state.firstReferee,
      secondReferee: this.state.secondReferee,
      lineJudge1: this.state.lineJudge1,
      lineJudge2: this.state.lineJudge2,
      scorekeeper: this.state.scorekeeper,
    });
  }


  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>

        <Text style={styles.title}>
          Referees of the game
        </Text>

        <TextInput   value={this.state.firstReferee} onChangeText={(text) => this.setState({firstReferee: text})}
          placeholder={'First Referee'} placeholderTextColor="black"  style={styles.input}/>

        <TextInput   value={this.state.secondReferee} onChangeText={(text) => this.setState({secondReferee: text})}
          placeholder={'Second Referee'} placeholderTextColor="black" style={styles.input}/>

        <TextInput   value={this.state.lineJudge1} onChangeText={(text) => this.setState({lineJudge1: text})}
          placeholder={'Line Judge 1'} placeholderTextColor="black" style={styles.input}/>

        <TextInput   value={this.state.lineJudge2} onChangeText={(text) => this.setState({lineJudge2: text})}
          placeholder={'Line Judge 2'} placeholderTextColor="black" style={styles.input}/>

        <TextInput   value={this.state.scorekeeper} onChangeText={(text) => this.setState({scorekeeper: text})}
          placeholder={'Scorekeeper'} placeholderTextColor="black" style={styles.input}/>

        <TouchableOpacity onPress={this.handleContinue.bind(this)}>
          <Text style={styles.button}>
            Continue
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView >
    )
  }
}

export default connect (mapStateToProps, mapDispatchToProps) (Referees)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#edf3fc'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  }
});
