import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import Substitutions from './Substitutions'
import TimeoutsComponent from './TimeoutsComponent'
import { RNNumberStepper } from 'react-native-number-stepper';
import SendData from './SendData'
import Set3 from './Set3'
import {createStackNavigator, TabNavigator, StackActions, NavigationActions, createMaterialTopTabNavigator, withNavigation} from 'react-navigation';

import * as firebase from 'firebase'

var pointsA=0
var pointsB=0

class Points extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timeOfSet: '',
      pointsA: 0,
      pointsB: 0,
      setFinished: false,
      increment: 1,
      buttonsBackgroundColor: '#357FC0',
      labelBackgroundColor: '#4098E0',
    }
  }

  static navigationOptions = {
    title: "Set 2"
  }



  checkPointsA(PointsA){
    this.setState({
      pointsA: PointsA
    })

    pointsA=PointsA

    if (PointsA>=1 && Math.abs(PointsA-this.state.pointsB)>=2) {
      firebase.database().ref(this.props.navigation.getParam('nameCompetion')
                + '/' + this.props.navigation.getParam('matchNo') + '/' + 'Set2' + '/' + 'Points').set({

                  PointsA: PointsA,
                  PointsB: this.state.pointsB,
      })


      this.setState({
        setFinished: true,
        increment: 0,
        buttonsBackgroundColor: '#cde2f4',
        labelBackgroundColor: '#7d92a3',
      })
    }
  }

  checkPointsB(PointsB){
    this.setState({
      pointsB: PointsB
    })

    pointsB=PointsB

    if (PointsB>=5 && Math.abs(PointsB-this.state.pointsA)>=2) {
      firebase.database().ref(this.props.navigation.getParam('nameCompetion')
                + '/' + this.props.navigation.getParam('matchNo') + '/' + 'Set2' + '/' + 'Points').set({

                  PointsA: this.state.pointsA,
                  PointsB: PointsB
      })

      this.setState({
        setFinished: true,
        increment: 0,
        buttonsBackgroundColor: '#cde2f4',
        labelBackgroundColor: '#7d92a3'
      })
    }
  }


  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.container}>
          <TextInput value={this.state.timeOfSet} onChangeText={(text) => this.setState({timeOfSet: text})}
            placeholder={'Starting time of the set'} placeholderTextColor="black"  style={styles.input}/>
        </View>

        <Text style={styles.title}>Points</Text>

         <View style={styles.row}>
           <Text style={styles.text}>Team A</Text>

           <View style={styles.stepper}>
             <RNNumberStepper buttonsBackgroundColor={this.state.buttonsBackgroundColor}
               labelBackgroundColor={this.state.labelBackgroundColor}
               stepValue={this.state.increment} onChange={(pointsA) => this.checkPointsA(pointsA)}/>
           </View>
         </View>

         <View style={styles.row}>
           <Text style={styles.text}>Team B</Text>

           <View style={styles.stepper}>
             <RNNumberStepper buttonsBackgroundColor={this.state.buttonsBackgroundColor}
               labelBackgroundColor={this.state.labelBackgroundColor}
               stepValue={this.state.increment} onChange={(pointsB) => this.checkPointsB(pointsB)}/>
           </View>
         </View>

         {this.state.setFinished ? <ContinueButton handleContinue={this.props.screenProps.handleContinue}/> : null }

     </ScrollView>
    )
  }
}


const TabMenu = createMaterialTopTabNavigator({
    Points: { screen: Points },
    Timeouts: { screen: TimeoutsComponent },
    Subs: {screen: Substitutions}
  });


class Set2 extends React.Component{
  constructor(props) {
    super(props)
    }

  handleContinue(){

    this.props.navigation.navigate('Set3',
        {nameCompetion: this.props.navigation.getParam('nameCompetion'), matchNo: this.props.navigation.getParam('matchNo')})
  }

  render() {
    return (
      <TabMenu screenProps={{ pointsAG: pointsA, pointsBG: pointsB, handleContinue: this.handleContinue.bind(this)}}/>
    );
  }
}

export default withNavigation (Set2)


class ContinueButton extends React.Component{

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.props.handleContinue}>
           <Text style={styles.button}>
             Continue to second set
           </Text>
         </TouchableOpacity>
       </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer:{
    paddingBottom: 10,
    paddingVertical: 5,
  },
  container:{
    marginTop: 50,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  stepper:{
    marginRight: 10
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 5,
    textAlign: 'center'
  },
  text: {
    fontSize: 27,
    fontWeight: 'bold',
    marginBottom: 20,
    marginLeft: 10,
    marginTop: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 25,
    marginTop: 5
  },
  input: {
    width: 330,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  }
});
