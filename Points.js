import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import Substitutions from './Substitutions'
import TimeoutsComponent from './TimeoutsComponent'
import { RNNumberStepper } from 'react-native-number-stepper';
import SendData from './SendData'
import Set2 from './Set2'
import {createStackNavigator, TabNavigator, StackActions, NavigationActions, createMaterialTopTabNavigator, withNavigation} from 'react-navigation';

import * as firebase from 'firebase'

import { connect} from "react-redux";
import {bindActionCreators} from 'redux'
import { addPointsA } from "./actions/index";
import { addPointsB } from "./actions/index";

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({addPointsA: addPointsA, addPointsB: addPointsB}, dispatch)
  }
}

const mapStateToProps = state => {
  return { currentState: state };
};

class Points extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timeOfSet: '',
      increment: 1,
      setFinished: false,
      buttonsBackgroundColor: '#357FC0',
      labelBackgroundColor: '#4098E0',
    }
  }

  static navigationOptions = {
    title: "Set 1"
  }


  checkPointsA(PointsA){

     console.log(this.props.currentState.Set1);
    this.props.addPointsA(PointsA)

    if (PointsA>=5 && Math.abs(PointsA-this.props.currentState.Set1.pointsB)>=2) {
      firebase.database().ref(1
                + '/' + 1 + '/' + 'Set1' + '/' + 'Points').set({

                  PointsA: PointsA,
                  PointsB: this.props.currentState.Set1.pointsB,
      })

      this.setState({
        setFinished: true,
        buttonsBackgroundColor: '#cde2f4',
        labelBackgroundColor: '#7d92a3',
      })
    }
  }

  handleContinue(){
    this.props.navigation.navigate("Set2")
  }

  checkPointsB(PointsB){
    this.props.addPointsB(PointsB)


    if (PointsB>=5 && Math.abs(PointsB-this.props.currentState.Set1.pointsA)>=2) {
      firebase.database().ref(this.props.currentState.mainInfo[0].nameCompetion
                + '/' + this.props.currentState.mainInfo[0].matchNo + '/' + 'Set2' + '/' + 'Points').set({

                  PointsA: this.props.currentState.Set1.pointsA,
                  PointsB: PointsB
      })

      this.setState({
        setFinished: true,
        increment: 0,
        buttonsBackgroundColor: '#cde2f4',
        labelBackgroundColor: '#7d92a3'
      })
    }
  }


  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.container}>
          <TextInput value={this.state.timeOfSet} onChangeText={(text) => this.setState({timeOfSet: text})}
            placeholder={'Starting time of the set'} placeholderTextColor="black"  style={styles.input}/>
        </View>

        <Text style={styles.title}>Points</Text>

         <View style={styles.row}>
           <Text style={styles.text}>Team A</Text>

           <View style={styles.stepper}>
             <RNNumberStepper buttonsBackgroundColor={this.state.buttonsBackgroundColor}
               labelBackgroundColor={this.state.labelBackgroundColor}
               stepValue={this.state.increment} onChange={(pointsA) => this.checkPointsA(pointsA)}/>
           </View>
         </View>

         <View style={styles.row}>
           <Text style={styles.text}>Team B</Text>

           <View style={styles.stepper}>
             <RNNumberStepper buttonsBackgroundColor={this.state.buttonsBackgroundColor}
               labelBackgroundColor={this.state.labelBackgroundColor}
               stepValue={this.state.increment} onChange={(pointsB) => this.checkPointsB(pointsB)}/>
           </View>
         </View>

         {this.state.setFinished ?
           <TouchableOpacity onPress={this.handleContinue.bind(this)}>
             <Text style={styles.button}>
               Continue
             </Text>
           </TouchableOpacity> : null }

     </ScrollView>
    )
  }
}


export default connect(mapStateToProps, mapDispatchToProps) (Points)

const styles = StyleSheet.create({
  contentContainer:{
    paddingBottom: 10,
    paddingVertical: 5,
  },
  container:{
    marginTop: 50,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  stepper:{
    marginRight: 10
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 5,
    textAlign: 'center'
  },
  text: {
    fontSize: 27,
    fontWeight: 'bold',
    marginBottom: 20,
    marginLeft: 10,
    marginTop: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 25,
    marginTop: 5
  },
  input: {
    width: 330,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  }
});
