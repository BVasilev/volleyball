export const addMainInfo = info => ({
  type: "ADD_INFO",
  payload : info
})

export const addReferees = referees => ({
  type: "ADD_REFEREES",
  payload : referees
})

export const addPlayersA = playersA => ({
  type: "ADD_PLAYERS_A",
  payload : playersA
})

export const addPlayersB = playersB => ({
  type: "ADD_PLAYERS_B",
  payload : playersB
})

export const addServiceOrderTeamA = serviceA => ({
  type: "ADD_SERVICEORDER_A",
  payload : serviceA
})

export const addServiceOrderTeamB = serviceB => ({
  type: "ADD_SERVICEORDER_B",
  payload : serviceB
})

export const addPointsA = pointsA => ({
  type: "ADD_POINTS_A",
  payload : pointsA
})

export const addPointsB = pointsB => ({
  type: "ADD_POINTS_B",
  payload : pointsB
})

export const addSubA = subA => ({
  type: "ADD_SUB_A",
  payload : subA
})

export const addSubB = subB => ({
  type: "ADD_SUB_B",
  payload : subB
})

export const timeoutA = timeout => ({
  type: "TIMEOUT_A",
  payload : timeout
})

export const timeoutB = timeout => ({
  type: "TIMEOUT_B",
  payload : timeout
})
