import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator, StackActions, NavigationActions, createMaterialTopTabNavigator} from 'react-navigation';
import { connect } from "react-redux";
import { addPlayersB } from "./actions/index";
import * as firebase from 'firebase'

const mapDispatchToProps = dispatch => {
  return {
    addPlayersB: playersB => dispatch(addPlayersB(playersB))
  }
}

const mapStateToProps = state => {
  return { currentState: state };
};

class TeamB extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      no: '',
      name: '',
      no2: '',
      name2: '',
      no3: '',
      name3: '',
      no4: '',
      name4: '',
      no5: '',
      name5: '',
      no6: '',
      name6: '',
      no7: '',
      name7: '',
      no8: '',
      name8: '',
      no9: '',
      name9: '',
      no10: '',
      name10: '',
      no11: '',
      name11: '',
      no12: '',
      name12: '',
    }
  }

  static navigationOptions = {
    title: "Team B"
  }


  handleContinue(){
     this.props.navigation.navigate('ServiceOrderTeamA')

     firebase.database().ref(this.props.currentState.mainInfo[0].nameCompetion
              + '/' + this.props.currentState.mainInfo[0].matchNo + '/' + 'Players_SignUp_TeamB').set({

      Player1: this.state.no + '_' + this.state.name,
      Player2: this.state.no2 + '_' + this.state.name2,
      Player3: this.state.no3 + '_' + this.state.name3,
      Player4: this.state.no4 + '_' + this.state.name4,
      Player5: this.state.no5 + '_' + this.state.name5,
      Player6: this.state.no6 + '_' + this.state.name6,
      Player7: this.state.no7 + '_' + this.state.name7,
      Player8: this.state.no8 + '_' + this.state.name8,
      Player9: this.state.no9 + '_' + this.state.name9,
      Player10: this.state.no10 + '_' + this.state.name10,
      Player11: this.state.no11 + '_' + this.state.name11,
      Player12: this.state.no12 + '_' + this.state.name12,
    })

    this.props.addPlayersB({
      Player1: this.state.no + '_' + this.state.name,
      Player2: this.state.no2 + '_' + this.state.name2,
      Player3: this.state.no3 + '_' + this.state.name3,
      Player4: this.state.no4 + '_' + this.state.name4,
      Player5: this.state.no5 + '_' + this.state.name5,
      Player6: this.state.no6 + '_' + this.state.name6,
      Player7: this.state.no7 + '_' + this.state.name7,
      Player8: this.state.no8 + '_' + this.state.name8,
      Player9: this.state.no9 + '_' + this.state.name9,
      Player10: this.state.no10 + '_' + this.state.name10,
      Player11: this.state.no11 + '_' + this.state.name11,
      Player12: this.state.no12 + '_' + this.state.name12,
    });
  }


  render() {
    return (
      <View>
      <ScrollView contentContainerStyle={styles.contentContainer}>

        <Text style={styles.title}>Team B Sign Up sheet</Text>

         <View style={styles.row}>
           <TextInput value={this.state.no}
             onChangeText={(text) => this.setState({no: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name}
               onChangeText={(text) => this.setState({name: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>

         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no2: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name2: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no3: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name3: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no4: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name4: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no5: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name5: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no6: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name6: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no7: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name7: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no8: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name8: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no9: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name9: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no10: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name10: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no11: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name11: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.row}>
           <TextInput value={this.state.no2}
             onChangeText={(text) => this.setState({no12: text})}
             placeholder={'Nº'} placeholderTextColor="black"  style={styles.inputFields}/>

           <TextInput value={this.state.name2}
               onChangeText={(text) => this.setState({name12: text})}
               placeholder={'Name of the player'} placeholderTextColor="black"  style={[styles.inputFields, styles.bigInputField]}/>
         </View>
         <View style={styles.container}>
           <TouchableOpacity onPress={this.handleContinue.bind(this)}>
             <Text style={styles.button}>
               Continue
             </Text>
           </TouchableOpacity>
         </View>
     </ScrollView>
   </View>
    )
  }
}


export default connect (mapStateToProps, mapDispatchToProps) (TeamB)


const styles = StyleSheet.create({
  contentContainer:{
    paddingBottom: 10,
    paddingVertical: 5,
  },
  container:{
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 5,
    textAlign: 'center'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 25,
    marginTop: 5
  },
  inputFields: {
    flex: 1,
    height: 44,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginLeft: 5,
    textAlign: 'center',
  },
  bigInputField: {
    marginLeft: 5,
    marginRight: 5,
    flex: 3,
    textAlign: 'center'
  }
});
