const initialState = {
  mainInfo: {},
  referees: {},
  playersA: {},
  playersB: {},
  serviceOrderTeamA: {},
  serviceOrderTeamB: {},
  Set1: {
    pointsA: 0,
    pointsB: 0,
    Subs: {
      TeamA: [],
      TeamB: []
    },
    Timeouts:{
      TeamA: [],
      TeamB: []
    }
  }
}

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_INFO":
        return {...state, mainInfo: action.payload}
    case "ADD_REFEREES":
        return {...state, referees: action.payload}
    case "ADD_PLAYERS_A":
        return {...state, playersA: action.payload}
    case "ADD_PLAYERS_B":
        return {...state, playersB: action.payload}
    case "ADD_SERVICEORDER_A":
        return {...state, serviceA: action.payload}
    case "ADD_SERVICEORDER_B":
        return {...state, serviceB: action.payload}
    case "ADD_POINTS_A":
        return {...state, Set1: {...state.Set1, pointsA: action.payload}}
    case "ADD_POINTS_B":
        return {...state, Set1: {...state.Set1, pointsB: action.payload}}
    case "ADD_SUB_A":
        return {...state, Set1: {...state.Set1, Subs: {...state.Set1.Subs, TeamA: [...state.Set1.Subs.TeamA, action.payload]}}}
    case "ADD_SUB_B":
        return {...state, Set1: {...state.Set1, Subs: {...state.Set1.Subs, TeamB: [...state.Set1.Subs.TeamB, action.payload]}}}
    case "TIMEOUT_A":
        return {...state, Set1: {...state.Set1, Timeouts: {...state.Set1.Timeouts, TeamA: [...state.Set1.Timeouts.TeamA, action.payload]}}}
    case "TIMEOUT_B":
        return {...state, Set1: {...state.Set1, Timeouts: {...state.Set1.Timeouts, TeamB: [...state.Set1.Timeouts.TeamB, action.payload]}}}
    default:
      return state;
  }
}

export default rootReducer
