import React from 'react';
import {
  View,
  Text,
  Button,
  Container,
  Content,
  StyleSheet,
  TextInput,
  Input,
  Alert,
  Header,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import {createStackNavigator, StackActions, NavigationActions, withNavigation} from 'react-navigation';

import TimerCountdown from 'react-native-timer-countdown';

import * as firebase from 'firebase'

import { connect} from "react-redux";
import {bindActionCreators} from 'redux'
import {timeoutA, timeoutB} from './actions/index'


//TO do
//need to prevetn user from mkaing more than 2 timeouts per team

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({timeoutA: timeoutA, timeoutB: timeoutB}, dispatch)
  }
}

const mapStateToProps = state => {
  return { currentState: state };
};

class TimeoutsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timeoutA: false,
      timeoutB: false,
      countTimeouts: 1
    }
  }

  static navigationOptions = {
    title: "Timeouts"
  }

  handleGetTimeoutA(){
    this.setState({
      timeoutA: true,
      timeoutRequest: true
    })

    var timeouts = this.state.countTimeouts++
    firebase.database().ref(this.props.navigation.getParam('nameCompetion')
              + '/' + this.props.navigation.getParam('matchNo') + '/' + 'Set1' + '/'
              + 'Timeouts' + '/' + 'TeamA' + '/' + timeouts).set({

                PointsA: this.props.currentState.Set1.pointsA,
                PointsB: this.props.currentState.Set1.pointsB,
    })

    this.props.timeoutA({
      PointsA: this.props.currentState.Set1.pointsA,
      PointsB: this.props.currentState.Set1.pointsB,
    })
  }

  handleGetTimeoutB(){
    this.setState({
      timeoutB: true,
      timeoutRequest: true
    })

    var timeouts = this.state.countTimeouts++
    firebase.database().ref(this.props.navigation.getParam('nameCompetion')
              + '/' + this.props.navigation.getParam('matchNo') + '/' + 'Set1' + '/'
              + 'Subs' + '/' + 'TeamB' + '/' + 'Timeouts' + timeouts).set({

                PointsA: this.props.currentState.Set1.pointsA,
                PointsB: this.props.currentState.Set1.pointsB,
    })

    this.props.timeoutB({
      PointsA: this.props.currentState.Set1.pointsA,
      PointsB: this.props.currentState.Set1.pointsB,
    })
  }


  render() {
    return (
      <KeyboardAvoidingView style={styles.contentContainer} behavior="padding" enabled>

        <Text style={styles.title}>
          Timeouts
        </Text>

        <View style={styles.container}>
        <Text> Team A </Text>

          <View style={styles.row}>
            <View style={styles.container}>
              {this.state.timeoutRequest ? null
                : <TouchableOpacity onPress={this.handleGetTimeoutA.bind(this)}>
                   <Text style={[styles.button, styles.inputFields]}>
                     Get a timeout
                   </Text>
                 </TouchableOpacity>
              }

             </View>

             {this.state.timeoutA ?
             <TimerCountdown onTimeElapsed={() => this.setState({timeoutA: false, timeoutRequest: false})} initialSecondsRemaining={1000*30} style={[styles.inputFields, styles.bigInputField]}/>
             : null}
        </View>

        </View>

        <View style={styles.container}>
        <Text> Team B </Text>

          <View style={styles.row}>
            <View style={styles.container}>
              {this.state.timeoutRequest ? null
                : <TouchableOpacity onPress={this.handleGetTimeoutB.bind(this)}>
                   <Text style={[styles.button, styles.inputFields]}>
                     Get a timeout
                   </Text>
                 </TouchableOpacity>
              }
             </View>

             {this.state.timeoutB ?
             <TimerCountdown onTimeElapsed={() => this.setState({timeoutB: false, timeoutRequest: false})} initialSecondsRemaining={1000*30} style={[styles.inputFields, styles.bigInputField]}/>
             : null}
        </View>

        </View>

      </KeyboardAvoidingView >
    )
  }
}

const styles = StyleSheet.create({
  contentContainer:{
    paddingBottom: 10,
    paddingVertical: 5,
  },
  container:{
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 50
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    padding: 20,
    borderColor: 'black',
    backgroundColor: '#408af9'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 5,
    textAlign: 'center'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 25,
    marginTop: 5
  },
  inputFields: {
    flex: 1,
    height: 44,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginLeft: 5,
    textAlign: 'center',
  },
  bigInputField: {
    marginLeft: 5,
    marginRight: 5,
    flex: 2,
    textAlign: 'center'
  }
});

export default connect(mapStateToProps, mapDispatchToProps) (TimeoutsComponent)
