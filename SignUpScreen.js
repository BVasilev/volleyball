import React from 'react';
import { View, Text, Button, Container, Content, StyleSheet, TextInput,
         Alert, Header, TouchableOpacity, KeyboardAvoidingView  } from 'react-native';
import { createStackNavigator, StackActions } from 'react-navigation';
import * as firebase from 'firebase'

class SignUpScreen extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      email: '',
      password: '',
    }
  }

  static navigationOptions = {
    title: "Create an account"
  }

  resetNavigation = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Main' })],
      });
    this.props.navigation.dispatch(resetAction);
  }

  handleSignUp = () => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(this.resetNavigation)
      .catch((error) => alert(error))
  }

  render() {
    return(
      <KeyboardAvoidingView  style={styles.container} behavior="padding" enabled>

        <Text style={styles.title}> Please fill in the details </Text>

        <TextInput
          value={this.state.email}
          onChangeText={(email) => this.setState({ email })}
          placeholder={'Email'}
          style={styles.input}
        />
        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder={'Password'}
          secureTextEntry={true}
          style={styles.input}
        />

      <TouchableOpacity onPress={this.handleSignUp}>
            <Text style = {styles.button}>
               Sign Up
            </Text>
         </TouchableOpacity>
      </KeyboardAvoidingView >

    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#edf3fc'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 100
  },
  button: {
    overflow: 'hidden',
    borderRadius: 10,
    height: 44,
    marginBottom: 10,
    textAlign: 'center',
    borderWidth: 1,
    width: 300,
    padding: 12.5,
    borderColor: 'black',
    backgroundColor: '#408af9'
  }
});


export default SignUpScreen
